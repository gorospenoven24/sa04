-- 1
SELECT * FROM songs WHERE song_name LIKE "%d%"; 

--  2
SELECT * FROM songs WHERE length < 231;

--  3
SELECT albums.album_title, songs.song_name, songs.length FROM albums 
    JOIN songs ON albums.id = songs.album_id;

--  4
SELECT *  FROM artists 
    JOIN albums ON artists.id = albums.artist_id 
    WHERE album_title LIKE "%a%"; 

-- 5
SELECT * FROM albums ORDER BY album_title DESC LIMIT 3; 

-- 6
SELECT albums.album_title, songs.song_name  FROM albums 
    JOIN songs ON albums.id = songs.album_id 
    ORDER BY album_title  DESC,
    song_name ASC; 